<?php
if (!function_exists('is_localhost')){
 function is_localhost() {
        $whitelist = array( '127.0.0.1', '::1' );
        if( in_array( $_SERVER['REMOTE_ADDR'], $whitelist) ){
            return true;
        }
        else{
            return false;
        }
 }
}
 
 if(is_localhost()){
      include_once(__DIR__ . '/IModel.php');
 }
 else{
        include_once(__DIR__ . '/../IModel.php');
 }
class ReporteModelBamberger  extends IModel {
    public  function getInformacion(){
       $query=$this->conn->prepare("call sp_informacion()");
       $query->execute();
       $res=$query->fetchAll(PDO::FETCH_OBJ);
       echo json_encode($res[0]);
    }
}
