app.controller("listCtrl",function ($scope,$http,$mdDialog,$mdToast,opcionesService,$rootScope,$translate) 
 {
   $rootScope.section="list";
   $scope.index2Delete=new Array();
   $scope.completed=false;
   $scope.prospectos={};
   $scope.opService=opcionesService; 
   $scope.gotoBusqueda=function(){
       alert("goto busqueda");
   }
   $http.get("../php/api/getProspectos.php").then(function(response){
           $scope.prospectos=response.data.prospectos;   
           $scope.completed=true;
   });  
  $scope.eliminarProspectos = function(ev) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
          .title($translate.instant('TITLE_DELETE'))
              .textContent($translate.instant('CAPTION_DELETE'))
              .targetEvent(ev) 
              .ok($translate.instant('ACCEPT'))
              .cancel($translate.instant('CANCEL'));

    $mdDialog.show(confirm).then(function() {
        
            for(var i=0;i<=$scope.prospectos.length;i++){
              if(typeof  $scope.prospectos[i]!="undefined"){
                    if($scope.prospectos[i].delete==true){
                         $scope.index2Delete.push($scope.prospectos[i].id_prospecto); 
                         $scope.prospectos.splice(i,1);
                    }
                }
            }
            $http({method: 'POST',
              async : true,
                url: "../php/api/deleteMultiples.php",
                 data:JSON.stringify($scope.index2Delete),
                 headers: {'Content-type': 'application/json'}
            }).success(function (data, status, headers, config) {
                    $scope.mostrarToast('Eliminado correctamente');
            
           }).error(function (data, status, headers, config) {
                    alert(data);
         });
          
        }, function() {
     
    });
  };
  $scope.mostrarToast=function(mensaje){
        $mdToast.show($mdToast.simple().textContent(mensaje));
   };
      
});

