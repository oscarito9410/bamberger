<?php
if (!function_exists('is_localhost')){
 function is_localhost() {
        $whitelist = array( '127.0.0.1', '::1' );
        if( in_array( $_SERVER['REMOTE_ADDR'], $whitelist) ){
            return true;
        }
        else{
            return false;
        }
 }
}
 
 if(is_localhost()){
      include_once(__DIR__ . '/IModel.php');
 }
 else{
        include_once(__DIR__ . '/../IModel.php');
 }
class LoginModel extends IModel{
    
    public function isValid($nombre,$pass){
        try{
            $query=$this->conn->prepare("SELECT*FROM usuario WHERE nombre=:nombre");
            $query->bindParam(":nombre",$nombre);
            $query->execute();
            $usuario=$query->fetch(PDO::FETCH_ASSOC);
            if($query->rowCount()>0){

                /*
                $hash=  password_hash($pass, PASSWORD_DEFAULT);
                if(password_verify($usuario['password'], $hash)){
                    session_start();
                    $_SESSION['nombre_usuario']=$nombre;
                    echo 'ok';
                }*/

                if($usuario['password']==$pass)
                 {
                    session_start();
                    $_SESSION['nombre_usuario']=$nombre;
                    echo 'ok';
                }
                else{
                    $this->enviarError('Password Invalida');
                }

            }
            else{
                $this->enviarError('El nombre de usuario '. $nombre .' no esta registrado aun');
            }
        } catch (PDOException $ex) {
            $this->agregarStatusEnvio($ex->getMessage());
        }
        
    }
    
    
    
}
