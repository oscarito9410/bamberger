<?php
if (!function_exists('is_localhost')){
 function is_localhost() {
        $whitelist = array( '127.0.0.1', '::1' );
        if( in_array( $_SERVER['REMOTE_ADDR'], $whitelist) ){
            return true;
        }
        else{
            return false;
        }
 }
}
 if(is_localhost()){
      include_once(__DIR__ . '/IModel.php');
 }
 else{
        include_once(__DIR__ . '/../IModel.php');
 }
class ProspectoModel extends IModel {
   
    public function exitsEmpresa($json_data){
       if(is_string($json_data)){
            $data=  json_decode($json_data);
        }
        else{
            $data=$json_data;
        }
        $query=$this->conn->prepare("SELECT*FROM prospecto_empresa WHERE nombre_empresa LIKE CONCAT('%',:nombre_empresa,'%')");
        $query->bindParam(":nombre_empresa",utf8_decode($data->nombre_empresa));
        $res=$query->fetchAll(PDO::FETCH_OBJ);
        if(count($res)>0){
            $this->agregarStatusEnvio('POSIBLE DUPLICADO'.utf8_decode($data->nombre_empresa));
        }
        else{
            $this->agregarStatusEnvio('REVISANDO A'.utf8_decode($data->nombre_empresa));
        }
    }
    public  function getMateriales(){
            $query=$this->conn->prepare("SELECT*FROM material ORDER BY id_material");
            $query->execute();
            $res=$query->fetchAll(PDO::FETCH_OBJ);
            return json_encode($res);
    }
    public  function getVendedores(){
            $query=$this->conn->prepare("SELECT id_vendedor, nombre FROM vendedor  WHERE id_vendedor<>0 ORDER BY id_vendedor");
            $query->execute();
            $res=$query->fetchAll(PDO::FETCH_OBJ);
            return json_encode($res);
    }
    public function  getAllProspectos(){
        $query= $this->conn->prepare("SELECT*FROM prospecto_empresa");
        $query->execute();
        $res=$query->fetchAll(PDO::FETCH_OBJ);
        foreach($res as $prospecto){
            $prospecto->nombre_empresa=  utf8_encode($prospecto->nombre_empresa);
            $prospecto->nombre_contacto=  utf8_encode($prospecto->nombre_contacto);
            $prospecto->direccion=  utf8_encode($prospecto->direccion);
            $prospecto->colonia=  utf8_encode($prospecto->colonia);
            $prospecto->ciudad_estado=  utf8_encode($prospecto->ciudad_estado);
            $prospecto->sitio_web=  utf8_encode($prospecto->sitio_web);
            $prospecto->comentarios=  utf8_encode($prospecto->comentarios);
            $prospecto->telefono=  utf8_encode($prospecto->telefono);
            $prospecto->delete=false;
            $prospectos=array("prospectos"=>$res);
           
        }
         $prospectos=array("prospectos"=>$res);
         return json_encode($prospectos);
   }
    public function deleteProspecto($id_prospecto){
       $query=$this->conn->prepare("DELETE FROM prospecto_empresa WHERE id_prospecto=:id_prospecto");
       $query->bindParam(":id_prospecto",$id_prospecto);
       $query->execute();
       echo 'Borrado';
    }
    public function agregarProspecto($json_encode_data){
       if(is_string($json_encode_data)){
            $data=  json_decode($json_encode_data);
        }
        else{
            $data=$json_encode_data;
        }
       
       if(!empty($data->nombre_empresa) &&!empty($data->nombre_contacto) && !empty($data->direccion))   
        {
           try{
                    $query=  $this->conn->prepare("INSERT INTO prospecto_empresa(nombre_empresa, nombre_contacto,  direccion,
                    fecha_asignacion, colonia, ciudad_estado, telefono, sitio_web, capacidad, comentarios,email,
                    id_asignado,source)
                    VALUES(:nombre_empresa,:nombre_contacto,:direccion,:fecha_asignacion,:colonia,:estado,:telefono,:sitio_web,:capacidad,:comentarios,:email,:asignado,:source);");
                    $query->bindParam(":nombre_empresa",  utf8_decode(strtoupper($data->nombre_empresa)));
                    $query->bindParam(":nombre_contacto", utf8_decode($data->nombre_contacto));
                    $query->bindParam(":direccion",  utf8_decode($data->direccion));
                    $query->bindParam(":colonia",  utf8_decode($data->colonia));
                    $query->bindParam(":fecha_asignacion",$data->fecha_asignacion);
                    $query->bindParam(":estado",  utf8_decode($data->estado));
                    $query->bindParam(":telefono",  utf8_decode($data->telefono));
                    $sitio=isset($data->sitio)? utf8_decode($data->sitio):'';
                    $asignado=  isset($data->asignado)? $data->asignado:0;
                    $query->bindParam(":asignado",$asignado);
                    $query->bindParam(":sitio_web", $sitio);
                    $query->bindParam(":email", $data->email);
                    $query->bindParam(":comentarios",  utf8_decode($data->comentario));
                    $query->bindParam(":capacidad",  utf8_decode($data->capacidad));
                    $query->bindParam(":source",  utf8_decode($data->source));
                    $query->execute();
                    $last_id=$this->conn->lastInsertId();
                    if(!is_string($json_encode_data)){
                        foreach ($data->materiales as $material){
                                $queryMaterial=$this->conn->prepare("INSERT INTO prospecto_material(id_prospecto,id_material) VALUES (:id_prospecto,:id_material)");
                                $queryMaterial->bindParam(":id_prospecto",$last_id);
                                $queryMaterial->bindParam(":id_material",$material);
                                $queryMaterial->execute();
                        }
                     }
                    if(is_string($json_encode_data)){
                       $this->agregarStatusEnvio('La empresa '.  utf8_encode(utf8_decode($data->nombre_empresa)).' ha sido registrada en la BD');
                    }
                    else{
                         echo 'La empresa '.  utf8_encode(utf8_decode($data->nombre_empresa)).' ha sido registrada en la BD';
                    }
           } catch (PDOException $ex) {
               if($ex->getCode()==23000){
                    if(is_string($json_encode_data)){
                       $this->agregarStatusEnvio('La empresa '.  utf8_encode(utf8_decode($data->nombre_empresa)).' ya esta registrada en la BD');
                    }
                    else{
                         echo 'La empresa '.  utf8_encode(utf8_decode($data->nombre_empresa)).' ya esta registrada en la BD';
                    }
                  
               }else{
                   if(is_string($json_encode_data)){
                       $this->agregarStatusEnvio($ex->getMessage());
                   }
                 
                  else{
                   echo $ex->getMessage();   
                  }
               }
              
           }
           
       
       }
    }
    public function asignarVendedor($id_prospecto,$id_vendedor){
      try{
         $query=$this->conn->prepare("UPDATE prospecto_empresa SET id_asignado=:id_asignado, fecha_asignacion=NOW() WHERE id_prospecto=:id_prospecto ");
         $query->bindParam(":id_prospecto",$id_prospecto);
         $query->bindParam(":id_asignado", $id_vendedor);
         $query->execute();
         echo 'Prospecto asignado correctamente';
      }  catch (PDOException $ex){
          echo $ex->getMessage();
      }
    }

    
    
   
    
   
    
}
