 var app=angular.module('appBm', ['ngMaterial','ngRoute','ngMask','pascalprecht.translate']);
 app.config(function($routeProvider,$translateProvider)
 {
   $translateProvider.translations('en', {
    OPCIONES:'OPTIONS',
    EN:'ENGLISH',
    ES:'SPANISH',
    IDIOMA:'LANGUAGE',
    INICIO: 'HOME',
    AGREGAR: 'ADD',
    BUSCAR: 'SEARCH',
    SUBIR: 'UPLOAD',
    LIST:'LIST',
    EXIT:'SIGN OUT',
    COMPANY_NAME:'Company name',
    CONTACT_NAME:'Contact name',
    STREET_NAME:'Street name',
    PHONE:'Phone',
    STREET_NAME_TWO:'Second Street name',
    STATE:'State',
    EMAIL:'Email',
    WEBSITE:'Website',
    PRODUCTS:'Products',
    ASSIGNED:'Assigned',
    DATE:'Date',
    SOURCE:'Source',
    CAPACITY:'Tons capacity',
    COMMENTS:'Comments',
    ADD_NEW_PROSPECT:'ADD NEW PROSPECT',
    ASSIGN_SELLER:'ASSIGN SELLER',
    SEARCH_RESULT:'SEARCH RESULT',
    SEARCH_PROSPECT:'SEARCH PROSPECT',
    CHOOSE_SELLER:'CHOOSE THE SELLER YOU WILL ASSIGN TO THE COMPANY',
    ACCEPT:'ACCEPT',
    CANCEL:'CANCEL',
    TITLE_DELETE:'Would you like to delete this company?',
    CAPTION_DELETE:'It will be completely removed from the DB',
    DELETED_OK:'Deleted successfully',
    SOURCES_LIST:{
       TELEMARKETING:'TELEMARKETING',
       LLAMADA:'PHONE CALL',
       INEGI:'INEGI DATABASE',
       PLASTIC_INDUSTRY:'PLASTIC INDUSTRY DIRECTORY',
       GEOPLASTIC:'GEOPLASTICT',
       WEB:'WEB SEARCH',
       ANIQ:'ANIQ DB',
       OTHER:'OTHER'
    },
    NEW_SOURCE:'ADD NEW SOURCE',
    WHERE_SOURCE:'Where does the prospect come from?',
    
  });  
   $routeProvider.when("/",{
       templateUrl:"../panel/index.html"
    });
   $routeProvider.when("/busqueda",
   {
       templateUrl:"../busqueda/index.html",
       controller  :"busquedaCtrl"
   });
   $routeProvider.when("/agregar",
   {
       templateUrl:"../agregar/index.html",
       controller  :"agregarCtrl"
   });
   $routeProvider.when("/list",
   {
       templateUrl:"../prospecto-list/index.html",
       controller  :"listCtrl"
   });
   
   $translateProvider.preferredLanguage('en');
 });
 app.service("controlService",function(){
    
   this.estados=["Aguascalientes","Baja California","Baja California Sur","Campeche",
                    "Chiapas","Chihuahua","Coahuila","Colima","Distrito Federal",
                    "Durango","Estado de México","Guanajuato","Guerrero","Hidalgo","Jalisco","Michoacán",
                    "Morelos","Nayarit","Nuevo León","Oaxaca","Puebla","Querétaro","Quintana Roo","San Luis Potosí",
                    "Sinaloa","Sonora","Tabasco","Tamaulipas","Tlaxcala","Veracruz","Yucatán","Zacatecas"];

 });
 app.directive('inputClear',inputClear);
 function inputClear() {
        return {
            restrict: 'A',
            compile: function (element, attrs) {
                var color = attrs.inputClear;
                var style = color ? "color:" + color + ";" : "";
                var action = attrs.ngModel + " = ''";
                element.after(
                    '<md-button class="animate-show md-icon-button md-accent"' +
                    'ng-show="' + attrs.ngModel + '" ng-click="' + action + '"' +
                    'style="position: absolute; top: 0px; right: -6px; margin: 13px 0px;">' +
                    '<div style="' + style + '">x</div>' +
                    '</md-button>');
            }
        };
 }
 app.service("opcionesService",function($mdSidenav,$location)
 {
   this.gotoInicio=function(){
       this.toggleLeft();
       $location.path("/");
      
   };
   this.gotoAgregar=function(){
       this.toggleLeft();
       $location.path("/agregar");
   };
   this.gotoCargar=function(){
       this.toggleLeft();
       alert("Función en construcción");
    };
    this.gotoBusqueda=function(){
        this.toggleLeft();
        $location.path("/busqueda");
    };
    this.gotoList=function(){
        this.toggleLeft();
        $location.path("/list");
    };
    this.gotoSalir=function(){
        this.toggleLeft();
        window.location.href="../logout.php";
    };
    this.toggleLeft = buildToggler('left');
    this.toggleRight = buildToggler('right');
 
    function buildToggler(componentId) {
      return function() {
        $mdSidenav(componentId).toggle();
    };
    
    };
      
});