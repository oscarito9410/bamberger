<?php
/**
 * Description of BusquedaModel
 *
 * @author Oscar
 */
if (!function_exists('is_localhost')){
 function is_localhost() {
        $whitelist = array( '127.0.0.1', '::1' );
        if( in_array( $_SERVER['REMOTE_ADDR'], $whitelist) ){
            return true;
        }
        else{
            return false;
        }
 }
}
 if(is_localhost()){
      include_once(__DIR__ . '/IModel.php');
 }
 else{
        include_once(__DIR__ . '/../IModel.php');
 }
 
class BusquedaModel extends IModel {
   
     
  
    public function initBusqueda($nombre_empresa){
    try{
        $queryBusqueda=$this->conn->prepare("SELECT prospecto_empresa.*, vendedor.nombre as asignado FROM prospecto_empresa INNER JOIN vendedor ON prospecto_empresa.id_asignado=vendedor.id_vendedor WHERE nombre_empresa LIKE CONCAT('%',:nombre_empresa,'%')");
        $queryBusqueda->bindParam(":nombre_empresa",$nombre_empresa);
        $queryBusqueda->execute();
        $res=$queryBusqueda->fetchAll(PDO::FETCH_OBJ);
        foreach($res as $prospecto){
            $prospecto->nombre_empresa=  utf8_encode($prospecto->nombre_empresa);
            $prospecto->nombre_contacto=  utf8_encode($prospecto->nombre_contacto);
            $prospecto->direccion=  utf8_encode($prospecto->direccion);
            $prospecto->colonia=  utf8_encode($prospecto->colonia);
            $prospecto->ciudad_estado=  utf8_encode($prospecto->ciudad_estado);
            $prospecto->sitio_web=  utf8_encode($prospecto->sitio_web);
            $prospecto->comentarios=  utf8_encode($prospecto->comentarios);
            $prospecto->telefono=  utf8_encode($prospecto->telefono);
        }
        $prospectos=array("prospectos"=>$res);
      
        return json_encode($prospectos);
        }
        catch (PDOException $ex){
            
            $this->enviarError($ex->getMessage());
        }
    }

    
}
