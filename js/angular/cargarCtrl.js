
app.controller("cargarCtrl",function ($scope,$http,$timeout,opcionesService) 
 {
     $scope.opService=opcionesService;
     $scope.seleccion={};
     $scope.isLoading=false;
     $scope.layoutCargado=false;
     $scope.isFile=false;
     
     
     $scope.guardarBD=function(){        
        $scope.isLoading=true;
        $scope.seleccion.nombre_archivo=$scope.nombre_archivo;
        $http({method: 'POST',
              async : true,
                url: "../php/api/send2Bd.php",
                 data:JSON.stringify($scope.seleccion),
                 headers: {'Content-type': 'application/json'}
        }).success(function (data, status, headers, config) {
            $scope.isLoading=false;
            alert(data);
            $("#progressMensaje").text(" ");
          }).error(function (data, status, headers, config) {
                    alert(data);
         });

      };
     $scope.analizar=function(){
               $scope.isLoading=true;
                var file_data = $('#csv').prop('files')[0];  
                if(typeof  file_data=="undefined"){
                   alert("Elige el archivo .csv que deseas cargar");
                 }
                var form_data = new FormData();                  
                form_data.append('csv', file_data);                           
                $.ajax({
                            url: '../php/api/encabezados.php', 
                            dataType: 'text', 
                            cache: false,
                            async: false,
                            contentType: false,
                            processData: false,
                            data: form_data,                         
                            type: 'post',
                            success: function(response){
            
                             $scope.data=JSON.parse(response);
                              console.log(response);
                               $scope.nombre_archivo=$scope.data.nombre_archivo;
                               $scope.isLoading=false;
                               $scope.layoutCargado=true;
                            },
                           error:function(response){
                                alert(response.responseText);
                                console.log(response);
                              
                           }
                 });
      };
     $scope.isEmpty=function(){
         return Object.keys($scope.seleccion).length===0;
      };
      var source = new EventSource("../php/api/eventSource.php");
      source.onmessage = function(event) 
      {   
      
               document.getElementById("progressMensaje").innerHTML=event.data;
      
          
           
                    
      };
      
});


