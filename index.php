<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html ng-app="appBm">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
    <link rel="stylesheet" href="css/login.css?id=1.5">
    <title>BAMBERGER</title>
</head>

<body ng-controller="loginCtrl" id="content">


    <div class="container-fluid">
       <div class="form-group animated bounceInDown">
           <img src="img/logo-transparente.png" width="400" height="101" class="img-responsive  center-block"/>
       </div>
        <div class="row padding-top-secciones ">
            
            <div class="col-xs-12 col-md-4 col-md-offset-4">
               
               
                <div class="text-center animated fadeIn">
                    
                    <form role="form" ng-submit="iniciarSesion()">
                        <div class="form-group">
                        
                        </div>
                        
                        <div class="form-group">
                                 <input type="text" class="form-control form-white" ng-model="usuario.nombre" placeholder="Usuario" required/>    
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control form-white" ng-model="usuario.password" placeholder="Password" required/>
                        </div>
                      
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary btn-block" value="Ingresar"/>
                        </div>
                       
                        <div class="form-group" style="text-align: left">
                            
                            <a href="#" class="azul-bamberger visible-md visible-lg">¿Olvidaste tu password?</a>
                            <div class="text-center visible-xs visible-sm">
                                <a href="#"   style="color: white">¿Olvidaste tu password?</a>
                            </div>
                            
                        </div>
                            
                    </form>
                   
                    
                </div>

            </div>
        </div>
    </div>
 

    </div>
    </div>
    <footer class="hidden-xs">
        <span>2016 BAMBERGER POLYMERS</span>
    </footer>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>
    <script src="js/jquery-1.11.2.js"></script>
    <script src="js/jquery.backstretch.js"></script>
    <script src="js/angular/loginCtrl.js"></script>
    <script>
        $(document).ready(function(){
         // if(window.innerWidth<780)
             $.backstretch("img/bg-2.jpg", {speed: 0});
        });
        </script>
</body>

</html>
