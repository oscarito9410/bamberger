app.service("busquedaService",function(){
    this.selectedId=1;
})
app.controller("busquedaCtrl",function($scope,$http,opcionesService,$mdToast,$mdDialog,busquedaService,$rootScope,$translate){
   function DialogController($scope, $mdDialog,busquedaService) {
          $scope.servicio=busquedaService;
          $scope.getVendedores=function(){ 
                $http.get("../php/api/getInfo.php",{
                     params:{tipo:'vendedor'}
                 }).then(function(response){
                     $scope.vendedores=response.data;
                     console.log(response.data);
                 });
            };
          $scope.getVendedores();       
          $scope.hide = function() {
              $mdDialog.hide($scope.id_vendedor);
            };
          $scope.cancel = function() {
              $mdDialog.cancel();
           };
     }
    $rootScope.section="busqueda";
    $scope.busquedaService=busquedaService;
    $scope.opService=opcionesService;
    $scope.querySearch=function(texto){
        console.log(texto);
        return $http.get("../php/api/busqueda.php",{params:{nombre_empresa:texto}}).then(function(response){
             console.log("data");
             console.log(response.data);
             return response.data.prospectos;
            
        });
    };
    
    $scope.selectedItemChange=function(item){
        $scope.selectedItem=item;
    };
    $scope.$watch('selectedItem',function(){
         angular.element(document.querySelector('#autocomplete')).blur();
    });
    $scope.asignarVendedor=function(ev){
            $mdDialog.show({
              controller: DialogController,
              templateUrl: '../html/dialogo-vendedor.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              clickOutsideToClose:true
            })
           .then(function(id_vendedor) {
              $scope.postAsignar(id_vendedor);
            }, function() {
        });
    };
    $scope.getToday=function(){
        
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //Enero es 0!

        var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd;
        } 
        if(mm<10){
            mm='0'+mm;
        } 
        var today = dd+'/'+mm+'/'+yyyy;
        return today;
    };
    $scope.getVendedorById=function(id_vendedor){
        $scope.items=["SIN ASIGNAR","ING. ALFONSO GARCIA","ALBERTO LOPEZ MALO","ALBERTO VELAZQUEZ","DAVY LOPEZ","JAIME GONZALEZ"
        ,"LEONARDO GONZALEZ","MANUEL BELTRAN","RICARDO GARZA","ROBERTO BREMONT"];
        return $scope.items[id_vendedor];
    }
    $scope.postAsignar=function(id_vendedor){
               $scope.asignados={};
               $scope.asignados.id_prospecto=$scope.selectedItem.id_prospecto;
               $scope.asignados.id_vendedor=id_vendedor;
               console.log("Asignados");
               console.log($scope.asignados);
               $http({method: 'POST',
                        async : true,
                        url: "../php/api/asignarVendedor.php",
                        data:JSON.stringify($scope.asignados),
                        headers: {'Content-type': 'application/json'}
               }).success(function (data, status, headers, config) {
                  $scope.mostrarToast(data);
                  $scope.selectedItem.fecha_asignacion=$scope.getToday();
                  $scope.selectedItem.id_asignado=id_vendedor;
                  $scope.selectedItem.asignado=$scope.getVendedorById(id_vendedor);
                  console.log($scope.selectedItem);
                
                }).error(function (data, status, headers, config) {
                          $scope.mostrarToast(data);
               });
     };   
    $scope.eliminarProspectos = function(ev) {
   
        var confirm = $mdDialog.confirm()
              .title($translate.instant('TITLE_DELETE'))
              .textContent($translate.instant('CAPTION_DELETE'))
              .targetEvent(ev) 
              .ok($translate.instant('ACCEPT'))
              .cancel($translate.instant('CANCEL'));

        $mdDialog.show(confirm).then(function() {
                $scope.id_prospecto=$scope.selectedItem.id_prospecto;
                console.log($scope.id_prospecto);
             
                    $http.get("../php/api/deleteProspecto.php",{params:{id_prospecto:$scope.id_prospecto}}).then(function(response){
                        $scope.mostrarToast($translate.instant('DELETED_OK'));
                          window.reload();
                    });


            }, function() {

        });
        
 
  };
     $scope.mostrarToast=function(mensaje){
        $mdToast.show($mdToast.simple().textContent(mensaje));
   };
});
