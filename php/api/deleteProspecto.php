<?php
require_once '../Model/ProspectoModel.php';
$prospecto = new ProspectoModel();
if(filter_has_var(INPUT_GET, 'id_prospecto')){
    $id_prospecto=  filter_input(INPUT_GET,'id_prospecto');
    $prospecto->deleteProspecto($id_prospecto);
}
