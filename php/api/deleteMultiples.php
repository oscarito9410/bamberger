<?php
require_once '../Model/ProspectoModel.php';
$prospecto = new ProspectoModel();
$jsonInput=file_get_contents('php://input');
if(!empty($jsonInput)){
    $prospectos=  json_decode($jsonInput);
    foreach ($prospectos as $item){
        $prospecto->deleteProspecto($item);
    }
}
