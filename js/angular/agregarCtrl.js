 app.controller("agregarCtrl",function ($scope,$http,$mdDialog, $timeout,$mdToast,$translate,controlService,opcionesService) 
 {
   $scope.vendedores={};
   $scope.materiales={};
   $scope.sources=[$translate.instant('SOURCES_LIST.TELEMARKETING'),$translate.instant('SOURCES_LIST.LLAMADA'),
   $translate.instant('SOURCES_LIST.INEGI'),$translate.instant('SOURCES_LIST.PLASTIC_INDUSTRY') ,
   $translate.instant('SOURCES_LIST.GEOPLASTIC'),$translate.instant('SOURCES_LIST.WEB'),$translate.instant('SOURCES_LIST.ANIQ'),
   $translate.instant('SOURCES_LIST.OTHER')];
   $scope.getVendedores=function(){ 
        $http.get("../php/api/getInfo.php",{
             params:{tipo:'vendedor'}
         }).then(function(response){
             $scope.vendedores=response.data;
             console.log(response.data);
         });
    };
    $scope.getMateriales=function(){
        $http.get("../php/api/getInfo.php",{
             params:{tipo:'material'}
         }).then(function(response){
             $scope.materiales=response.data;
             console.log(response.data);
         });
    };
 
    $scope.getVendedores();
    $scope.getMateriales();
    $scope.opService=opcionesService;
    $scope.prospecto={};
    $scope.estados=controlService.estados;
    $scope.onSubmitProspecto=function(){
     
      if(typeof $scope.prospecto.nombre_empresa==="undefined"){
        $scope.mostrarToast("Ingresa el nombre de la empresa");
      }
      else if(typeof $scope.prospecto.nombre_contacto==="undefined"){
           $scope.mostrarToast("Ingresa el nombre contacto");
      }
      else if(typeof $scope.prospecto.direccion==="undefined"){
         $scope.mostrarToast("Ingresa la dirección");
      }
      else if(typeof  $scope.prospecto.telefono==="undefined"){
          $scope.mostrarToast("Ingresa el telefono");
      }
      else if(typeof  $scope.prospecto.colonia==="undefined"){
            $scope.mostrarToast("Ingresa la colonia");
      }
      else{
          console.log($scope.prospecto);
          $http({
                          method: 'POST',
                          url: "../php/api/enviarTest.php",
                          data:JSON.stringify($scope.prospecto),
                            headers: {'Content-type': 'application/json'}
                        }).success(function (data, status, headers, config) {
                           alert(data);
                        }).error(function (data, status, headers, config) {
                            document.write(data);
          });
      }
      
      
    };
       $scope.onSource=function(ev){
        //Es el último elemento del array
         if(parseInt( $scope.prospecto.id_source)== parseInt ($scope.sources.length-1))
         {
             $scope.showPrompt(ev);
         }
         else{
             $scope.prospecto.source=$scope.sources[$scope.prospecto.id_source];
         }
    };
  $scope.showPrompt = function(ev) {
    var confirm = $mdDialog.prompt()
      .title($translate.instant('NEW_SOURCE'))
      .textContent($translate.instant('WHERE_SOURCE'))
      .placeholder('')
      .ariaLabel('otro prospecto')
      .targetEvent(ev)
      .ok($translate.instant('ACCEPT'))
      .cancel($translate.instant('CANCEL'));

    $mdDialog.show(confirm).then(function(result) {
      $scope.sources[$scope.sources.length]=result; //Agregamos el resultado al combobox
      $scope.prospecto.id_source= $scope.sources.length-1; //Lo mostramos como la opcion seleccionada
      $scope.prospecto.source=result;
    }, function() {
      // alert("Cancelar");
    });
  };
     $scope.mostrarToast=function(mensaje){
        $mdToast.show($mdToast.simple().textContent(mensaje));
   };
 });
