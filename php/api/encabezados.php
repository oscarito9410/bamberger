
<?php
require_once '../Model/Conexion.php';
    if(!empty($_FILES)){  
     $tmpName = $_FILES['csv']['tmp_name'];
     $handle = fopen($tmpName, "r");
     $count=0;
     $encabezados=array();
     while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if($count<1)
        {
              foreach($data as $encabezado){
                  array_push($encabezados, $encabezado);
              }
         }   
         $count++;
         
     }
     
    $nombre=guardarCSV();
    $bd_encabezado=getEncabezadosBd();
    $retorno=array(
        "encabezado"=>  utf8_converter($encabezados),
        "bd_encabezado"=>  utf8_converter($bd_encabezado),
        "nombre_archivo"=>$nombre,
        "total"=>($count-1)
        
    );
    echo json_encode($retorno);                
}

function utf8_converter($array)
    {
        array_walk_recursive($array, function(&$item, $key){
            if(!mb_detect_encoding($item, 'utf-8', true)){
                    $item = utf8_encode($item);
            }
        });

        return $array;
    }

function getEncabezadosBd(){
   
    $instancia = new Conexion();
    $conn=$instancia->getConexion();
   // $res=$conn->query("SELECT nombre_empresa,nombre_contacto,fecha_asignacion,direccion,colonia,ciudad_estado,telefono,sitio_web,capacidad,comentarios FROM prospecto LIMIT 1");
    $res=$conn->query("SELECT nombre_empresa FROM prospecto_empresa LIMIT 1");
    $columnas= array_keys($res->fetch(PDO::FETCH_ASSOC));
    return $columnas;
}

function guardarCSV(){
    if(is_uploaded_file($_FILES["csv"]["tmp_name"])){
                            $nombreDir="upload/";
                            $idUnico=  time();
                            $nombreFichero=$idUnico."-".$_FILES["csv"]["name"];
                            move_uploaded_file($_FILES["csv"]["tmp_name"], $nombreDir.$nombreFichero);
                            return $nombreFichero;
     }
}
