app.controller("opcionesCtrl",function($scope,$mdSidenav)
{
   $scope.gotoCargar=function(){
       $scope.toggleLeft();
       window.location.href="../cargar-excel/";
    };
    $scope.gotoBusqueda=function(){
        $scope.toggleLeft();
        window.location.href="../busqueda/";
    };
    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');
 
    function buildToggler(componentId) {
      return function() {
        $mdSidenav(componentId).toggle();
    }
    };
      
});