<?php
require_once '../Model/BusquedaModel.php';
if(!empty($_GET)){
    if(filter_has_var(INPUT_GET,'nombre_empresa')){
        $nombre_empresa=  filter_input(INPUT_GET,'nombre_empresa');
        $busqueda=new BusquedaModel();
        echo  $busqueda->initBusqueda($nombre_empresa);
    }
}
